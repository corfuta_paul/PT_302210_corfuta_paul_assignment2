package PT2020.demo.Assignment2;

import java.util.List;

public interface Strategy {
     public int addClient(List<Queue> queues, Client c);
}
