package PT2020.demo.Assignment2;

import java.util.ArrayList;
import java.util.List;

public class Scheduler {
	private List<Queue> queues;//cozile in care se adauga clienti
	private int maxNoServers;
	private int maxTaskPerServer;
	private Strategy strategy;
	
	public Scheduler(int maxNoServers,int maxTasksPerServer) {
		super();
		this.strategy = new ConcreteStrategyTime();
		this.queues = new ArrayList<Queue>(maxNoServers);
		this.maxNoServers = maxNoServers;
		this.maxTaskPerServer = maxTaskPerServer;;
		for(int i=0;i<this.maxNoServers;i++)
		{
			queues.add(new Queue(i+1,maxNoServers));	
		}	
	}
	
	public Strategy getStrategy() {
		return strategy;
	}

	public void setStrategy(Strategy strategy) {
		this.strategy = strategy;
	}

	public void dispatchClient(Client c) {//adaug clientul in coada care are timpul de asteptare mai mic
		int i=strategy.addClient(queues, c);
		queues.get(i).addClient(c);
		
	}
    public List<Queue> getQueues(){
    	return queues;
    }
}
