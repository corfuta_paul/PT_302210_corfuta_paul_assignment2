package PT2020.demo.Assignment2;

import java.util.Comparator;

public class SortByArrivalTime implements Comparator<Client> {

	public int compare(Client o1, Client o2) {
		return Integer.compare(o1.getT_arrival(), o2.getT_arrival());
	}

}
